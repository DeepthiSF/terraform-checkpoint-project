terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
// Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
}

// Create a VPC
resource "aws_vpc" "terraformCheckpointVPC" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "dcTerraformVPC"
  }
}

// Create a variable to define the cidr block 
variable "cidr_block" {
  type    = string
  default = "10.0.1.0/24"
}

// Create a subnet
resource "aws_subnet" "dcTerraformSubnet01" {
  vpc_id     = aws_vpc.terraformCheckpointVPC.id
  cidr_block = var.cidr_block
  tags = {
    Name = "dcTerraformSubnet01"
  }
  map_public_ip_on_launch = true
}

# Internet Gateway
resource "aws_internet_gateway" "dcTerraformIGW" {
  vpc_id = aws_vpc.terraformCheckpointVPC.id
  # comment
  tags = {
    Name = "dcTerraformIGW"
  }
}

# Route table
resource "aws_route_table" "dcTerraformRouteTable" {
  vpc_id = aws_vpc.terraformCheckpointVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dcTerraformIGW.id
  }

  tags = {
    Name = "dcTerraformRouteTable"
  }
}

# Route table association for subnet
resource "aws_route_table_association" "dcTerraformSubnetAssociation" {
  subnet_id      = aws_subnet.dcTerraformSubnet01.id
  route_table_id = aws_route_table.dcTerraformRouteTable.id
}

# Create a Security Group to associate with the EC2 instance
resource "aws_security_group" "dcTerraformSG" {
  name        = "dcTerraformSG"
  description = "Allow inbound traffic from the internet(0.0.0.0/0)"
  vpc_id      = aws_vpc.terraformCheckpointVPC.id

  # Inbound traffic from 0.0.0.0/0 via HTTP
  ingress {
    description = "HTTP inbound"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound traffic from 0.0.0.0/0 via SSH
  ingress {
    description = "SSH inbound"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound traffic from EC2 to outside
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "dcTerraformSG"
  }
}

# Create an EC2 Instance
resource "aws_instance" "dcTerraformInstance" {
  ami = "ami-002068ed284fb165b"
  instance_type = "t3.micro"

  # VPC/Subnet
  subnet_id = aws_subnet.dcTerraformSubnet01.id
  # Security Group
  vpc_security_group_ids = ["sg-0c60fb6170f91195d"]
  # the Public SSH key
  key_name = "deepthi-custom-vpc-ec2-key"
  
  tags = {
    Name = "dcTerraformInstance"
  }

  user_data = "${file("install.sh")}" 
}

output "terraformEC2PublicIPv4Address_id" {
  description = "Public IPv4 address of the EC2 instance"
  value = aws_instance.dcTerraformInstance.public_ip
}




